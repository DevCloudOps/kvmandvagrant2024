#!/usr/bin/env ruby
module Defaults

# this supports Redmine issus 4449

# some minimal clustered samba config. files for our CentOS 6.7 -- a 3 node cluster ...
# Vagrantfile should provision each node with /etc/samba and /etc/ctdb and /etc/sysconfig
# also an optional rc.local script and a rudimentary shell script for testing the install 

# note this module should reside alongside the Vagrantfile and its functions should be invoked
# from within the current working directory of the Vagrantfile (like all vagrant comands)
# i.e. require './defaults' should be indicated at the start of the Vagrantfile ...

require 'fileutils'

# john j. recommends Shellwords.escape("String") to safely use with a Bourne shell. 
# Note that a resulted string should be used unquoted and is not intended for use in double quotes nor in single quotes.
require 'shellwords' 

def self.vboxmanage(ip)
  print "#{ip}\n"
  ip = Shellwords.escape("#{ip}")
  print "#{ip}\n"
  vbox = `vboxmanage hostonlyif create`
  vbox = `vboxmanage hostonlyif ipconfig vboxnet0 --ip #{ip}`
end

def self.here2CTDB(path)
  if( !defined?(path) || path.length <= 0 )
     path = "../etc"
  end
  FileUtils.mkdir_p(["#{path}/sysconfig", "#{path}/ctdb"]) 

  filelist = {}
  outfile = "#{path}/ctdb/nodes" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    # ctdb does not look at /etc/hosts file (may need dns)? this should be the list of nodes in our cluster
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      192.168.33.10
      192.168.33.20
      192.168.33.30
      EOF
    end
  end
  outfile = "#{path}/ctdb/public_addresses" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    # evidently ctdb also needs its own set of IPs on the same subnet ...
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      192.168.33.210/24 eth1:1
      192.168.33.220/24 eth1:1
      192.168.33.230/24 eth1:1
      EOF
    end
  end
  outfile = "#{path}/sysconfig/ctdb" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      CTDB_NODES=/etc/ctdb/nodes
      CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses
      CTDB_RECOVERY_LOCK="/mnt/ctdb/.ctdb.lock"
      CTDB_MANAGES_SAMBA=yes
      CTDB_MANAGES_WINBIND=yes
      EOF
    end
  end
  return filelist
end

def self.here2Samba4(path)
  if( !defined?(path) || path.length <= 0 )
     path = "../etc"
  end
  FileUtils.mkdir_p("#{path}/samba4") 

  filelist = {}
  #outfile = "#{path}/samba4/vagrant_smbusers" ; filelist[outfile] = false
  #if( ! File.exist?(outfile) )
  #  filelist[outfile] = true
  #  file = File.open(outfile, "w") do |file|
  #    file << <<-EOF
  #    root = administrator admin vagrant
  #    nobody = guest pcguest smbguest
  #  EOF
  # end
  #end

  outfile = "#{path}/samba4/lmhosts67" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      127.0.0.1 localhost
      192.168.33.10  cos67.10
      192.168.33.20  cos67.20
      192.168.33.30  cos67.30
      192.168.33.70  win7
      192.168.33.81  win8.1
      192.168.33.100 win10
      EOF
    end
  end
  outfile = "#{path}/samba4/clustered_samba.conf" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      [global]
      comment = clustered samba
      clustering = yes
      # if and only if NO windows systems are configure as WINS servers, samba can be this network's WINS server 
      wins support = yes
      browsable = yes
      # disable samba password for simpler testing?
      # security = share (depracated!)
      security = user
      map to guest = Bad User 
      guest ok = yes
  
      # avoid CUPS errors:
      load printers = no
      printcap name = /dev/null
      disable spoolss = yes

      name resolve order = hosts lmhosts
      # client lanman auth = no
      # workgroup = 
      # netbios name = clustered-samba

      [vagrant]
      comment = test share of vagrant home file-sys privs.
      path = /home/vagrant
      public = yes
      ea support = yes
      writeable = no
      read only = yes
      inherit acls = no
      inherit permissions = no
      # remove all permissions:
      create mask = 0000
      # add read-only permissions for all 
      force create mode = 0111

      # this enforces passward protection
      # valid user = vagrant
      EOF
    end
  end
  return filelist
end

def self.here2sh(path)
  if( !defined?(path) || path.length <= 0 )
     path = "../etc"
  end
  FileUtils.mkdir_p("#{path}") 

  filelist = {}
  outfile = "#{path}/rc.local" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      #!/bin/sh
      #
      # This script will be executed *after* all the other init scripts.
      # You can put your own initialization stuff in here if you don't
      # want to do the full Sys V style init stuff.

      touch /var/lock/subsys/local

      # vagrantfile configs private non-routing IP for eth1 and should already be up ...
      ifup eth1
      # but ctdb want its own IPs, vagrantfile shell provision should take care of this foreach vhost ...
      # Vagrantfile provision should make permanant via /etc/sysconfig/network-scripts/ifcfg-eth1:1
      # ifconfig eth1:1 192.168.33.210 netmask 255.255.255.0 up
      # ifconfig eth1:1 192.168.33.220 netmask 255.255.255.0 up
      # ifconfig eth1:1 192.168.33.230 netmask 255.255.255.0 up
      #

      # selinux samba config (shoud be path indicated in samba.conf):
      semanage fcontext -a -t samba_share_t "/home/vagrant(/.*)?"

      # if ctdb is config's to "manage samba and winbind", just start ctdbd and hope for the best
      # unfortunately this prompts for root passwords (tbd -- how to bypass prompt)
      # onnode -p all service ctdb restart
      # onnode -p all service smb restart
      # onnode -p all service nmb restart
      # force clustered sambe restart ? ctdb appends log, so let's purge the logfile on restart:
      service ctdb stop
      mkdir -p /var/log/history >& /dev/null; mv /var/log/log.ctdb /var/log/history
      service network restart
      service ctdb start ; sleep 3 ; service smb restart ; sleep 3 ; service nmb restart
      # make sure there is a vagrant account user enabled (with no password)?
      smbpasswd -ena vagrant 
      # try to use samba client
      smbclient -NL cos67.10
      smbclient -NL cos67.20 
      smbclient -NL cos67.30
      # try to mount a samba share
      mkdir -p /samba/{cos67.10,cos67.20,cos67.30}
      mount -t cifs //cos67.10/vagrant /samba/cos67.10
      mount -t cifs //cos67.20/vagrant /samba/cos67.20
      mount -t cifs //cos67.30/vagrant /samba/cos67.30
      #
      echo "rc.local clustered samba sandbox test setup all done!"
      EOF
    end
  end
  outfile = "#{path}/clusterstat.sh" ; filelist[outfile] = false
  if( ! File.exist?(outfile) )
    filelist[outfile] = true
    file = File.open(outfile, "w") do |file|
      file << <<-EOF
      ping -c 1 192.168.33.10 ; ping -c 1 192.168.33.20 ; ping -c 1 192.168.33.30
      ping -c 1 192.168.33.81 ; ping -c 1 192.168.33.7
      # onnode all pidof ctdbd
      # onnode all netstat -tn | grep 4379
      ctdb status
      ctdb ping -n all
      ctdb ip
      smbcontrol smbd ping
      #
      # if /samba/vagrant has been mounted
      ls -alFq /samba/vagrant
      smbtorture -U vagrant //localhost/home/vagrant RAW-QFSINFO
      # assume vagrant user accnt needs no password (so may not need %):
      smbtorture -U vagrant //192.168.33.10/vagrant RAW-QFSINFO
      smbtorture -U vagrant //192.168.33.20/vagrant RAW-QFSINFO
      smbtorture -U vagrant //192.168.33.30/vagrant RAW-QFSINFO
      #
      # our sole windows 8.1 VM:
      smbtorture4 -Uvagrant% ncacn_np:192.168.33.81 RPC-WINREG
      EOF
    end
  end
  return filelist
end

def self.genFiles()
  path = ""
  allfiles = []
  files = here2Samba4(path) ; allfiles.push(files)
  files = here2CTDB(path) ; allfiles.push(files)
  files = here2sh(path) ; allfiles.push(files)
  # print("generated all default cluster provisioning files: #{allfiles}\n")
  return allfiles
end

end # module ClusterDefaults
